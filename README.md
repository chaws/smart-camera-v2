# Linaro Software Defined Camera (Smart Camera v2)

## How to build and run (short version)

On an `x86_64` or `aarch64` Linux machine (Debian/Ubuntu):

```
$ sudo apt install docker.io git make
$ git clone --recurse-submodules https://gitlab.com/jforissier/smart-camera-v2
$ cd smart-camera-v2
$ make
$ make deploy TO=<rockpi4-ip>
```
On the RockPi4 device:
```
# ./sc-demo install
# ./sc-demo test
[...]
Test mode: SUCCESS (10 objects detected)
[...]
# ./sc-demo start
[...]
Demo will be accessible at: http://192.168.0.12:5000/
```
See configuration options in `conf.mk`.

## How to build and run (longer version)

The Software Defined Camera project provides reference implementation to
demonstrate a typical object recognition stack, based on a microservice
architecture and suitable for cloud deployment. The main steps are:

1. Train an AI model from the Microsoft COCO (Common Objects in Context)
   dataset
2. Compile the trained model for an Aarch64 Linux target
3. Build Docker images for the microservice containers: video capture,
   AI inference, main application
4. Download prebuilt firmware and OS images for the target device (RockPi4).
   SDC runs on top of the Linaro Trusted Reference Stack software.
5. Deploy the containers onto the target device and run the demo.

Steps 1 and 2 can be performed either locally or in the cloud using Amazon Web
Services (SageMaker). It is also possible to download a prebuilt model to speed
things up.

### AI model training

When `TRAINING_MODE=local`, `make training` downloads about 20 GB of data from
https://cocodataset.org. The training phase can use a GPU if available (tested
on an `x86_64` Amazon EC2 instance with NVidia T4).

When `TRAINING_MODE=aws`, `make training` invokes AWS SageMaker services.

### AI model compilation

`make compilation` converts the trained model into a compiled model executable
on the Aarch64 target device. When `COMPILATION_MODE=local`, the build happens
locally. When `COMPILATION_MODE=aws`, AWS SageMaker is used instead.

When `COMPILATION_MODE=prebuilt`, a prebuilt model is downloaded. This
effectively allows skipping both the model training and compilation steps.

### Building the inference container image

`make inference` produces an `aarch64` Docker image that contains the compiled
model. The command can be run equally on an `x86_64` or `aarch64` machine, but
`x86_64` is much slower to build due to emulation (the `docker buildx` command
uses QEMU).

`make inference-image-save` saves the image as `out/sc-inference.tar.bz2`.

### Building the video capture container image

Use `make capture` and `make capture-image-save` to build the video capture
container and save it to disk as `out/sc-capture.tar.bz2`.

## How to run

### Obtaining image files for the RockPi4

Run `make rockpi4-trs-images-download` to retrieve the firmware and OS images
from the latest Linaro TRS build, then:
- `make rockpi4-trs-firmware-flash DEV=/dev/mmcblkX` to flash the firmware to
a microSD card
- `make rockpi4-trs-rootfs-flash DEV=/dev/sdX` to flash the OS to a USB stick.

### Installing on RockPi4

The container images may be copied to the target device using `scp` or any
other mean. The Makefile provides a convenient `deploy` target for that:

```
$ make deploy TO=<rockpi4-ip>
```
Then, log into the board as root and run `sc-demo install` to load the
containers into Docker.
```
# ./sc-demo install
```

### Running on RockPi4

The `sc-demo` script can be used to start and stop the demo. It also contains
a test mode which performs an end-to-end sanity check. It starts the capture and
inference containers, using a small video file as a source. Then it runs the SDC
application in test mode, returning a zero status if objects were detected or
non-zero otherwise.

### Amazon Web Services setup

Some actions in this project may require an AWS account, depending on how things
are configured in `conf.mk`. This sections will guide you through the account
setup process.

#### Pre-requisites for registration

This process can be skipped if you already have an AWS account that fulfills
the following requirements:

1. Ability to create and manage IAM users & groups
2. Ability to create and manage IAM roles
3. Ability to grant access for AWS services to IAM users/roles

If you don't already have an AWS account, you will need to sign up for one.
Note that at the time of writing this document, the
[AWS Free Tier](https://aws.amazon.com/free/) is sufficient to run the demo.

You will need:

1. An e-mail address to register
2. A valid credit card that can be charged and verified during registration
3. A contact phone (mobile number)

To register go to https://aws.amazon.com/.

#### Create IAM User

IAM (Identity and Access Management) allows to create a profile for use with
services on AWS. For the Smart Camera use case, a user and one or several roles
are needed. Only the user must be created manually; the makefiles will take
care of any subsequent step by using the user's credentials.

To create an IAM user:

1. Open the AWS dashboard
2. Search for and select `IAM - Manage access to AWS resources`
3. On the left-hand side pane select `Access management >> Users`
4. Click on the "Add users" button on the right hand side of the page
5. Pick a name for the IAM user, such as `SmartCamera` and click
   `Next`.
8. In `Set permissions`: select `Attach policies directly` then
   `Next` and finally `Create user`.
9. Select the newly created user, in the `Security credentials` tab
   click `Create access key`. Select `Command Line Interface (CLI)`.
   Tick `I understand the above recommendations [...]` and click
   `Next`.
10. Click `Create access key`
11. Take note of the following:
       - Access key ID. That's your `AWS_ACCESS_KEY_ID` in `aws.mk`.
       - Secret access key (hidden, click `Show` to reveal). This one
         is `AWS_SECRET_ACCESS_KEY` in `aws.mk`.
12. Click `Done`.
13. Back on the user summary page, take note of the numerical part of the
    user's ARN. That number is called `AWS_ACCOUNT_ID` in `aws.mk`.
14. Select the `Permissions` tab and
    click `Add permissions`/`Add inline policy`. Select the `JSON` tab.
15. Paste the following:
```
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": [
                    "ecr:*",
                    "iam:*",
                    "iot:*",
                    "s3:*",
                    "sagemaker:*"
                ],
                "Resource": "*"
            }
        ]
    }

```
15. Click `Review policy`
16. Give a name to the policy (such as `SmartCameraUserPolicy`) and click
    `Create policy`.
