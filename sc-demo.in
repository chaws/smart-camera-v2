#!/bin/bash
#
# sc-demo install
# Load the docker image files (sc-*.tar.bz2) into Docker
#
# sc-demo [start]
# Start the containers in detached mode.
#
# sc-demo test
# Start the capture and inference containers in detached mode. Start the
# application with self-test option and return a success (0) or failure (1)
# status.
#
# sc-demo stop
# Stop running containers (if any)
#
# sc-demo restart
# Stop, then start

#MODEL=yolov3
MODEL=tiny_yolov3
SOURCE=mp4
#SOURCE=usb_webcam
IP=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

stop() {
  echo "Stopping containers..."
  docker stop sc-capture-container sc-inference-container sc-application-container
  echo "Removing containers..."
  docker rm sc-capture-container sc-inference-container sc-application-container
}

_start_capture() {
  echo "Starting capture container..."
  case $SOURCE in
  mp4)
    #docker run -d --name sc-capture-container --network=host -v /home/root/traffic.mp4:/video.mp4 sc-capture ./rtsp-simple-server mp4.yml
    docker run -d --name sc-capture-container --network=host sc-capture ./rtsp-simple-server mp4.yml
    ;;
  usb_webcam)
    docker run -d --name sc-capture-container --network=host --device=@VIDEO_DEV@ sc-capture ./rtsp-simple-server usb_webcam.yml
    ;;
  *)
    echo "Error: unknown source: $SOURCE"
    exit 1
    ;;
  esac
}

start() {
  _start_capture
  echo "Starting inference container..."
  docker run -d --name sc-inference-container --network=host sc-inference -p 8080 -m ${MODEL}
  echo "Starting application container..."
  docker run -d --name sc-application-container --network=host sc-application -p 8080 -m ${MODEL} -ip 127.0.0.1 -r rtsp://127.0.0.1:8554/cam
  echo
  echo "Demo will be accessible at: http://${IP}:5000/"
}

start_test() {
  _start_capture
  echo "Starting inference container..."
  docker run -d --name sc-inference-container --network=host sc-inference -p 8080 -m ${MODEL}
  echo "Starting application in test mode..."
  docker run -it --rm --network=host sc-application -p 8080 -m ${MODEL} -ip 127.0.0.1 -r rtsp://127.0.0.1:8554/cam --disable_ui --test_mode
  RET=$?
  echo "Stopping containers..."
  docker stop sc-capture-container sc-inference-container
  echo "Removing containers..."
  docker rm sc-capture-container sc-inference-container
  return ${RET}
}

install() {
  set -e
  echo "Importing Docker images..."
  for f in sc-{application,capture,inference}.tar.bz2; do
    bzcat $f | docker load &
    pids="$pids $!"
  done
  wait $pids
  set +e
}

help() {
  echo "Usage: $0 install"
  echo "       $0 [start|stop|restart|test]"
}

case "$1" in
  install)
    install
    ;;
  ""|start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  test)
    start_test
    ;;
  help|-h)
    help
    ;;
  *)
    echo Error: unknown command: "$1"
    exit 1
    ;;
esac
