t := training

training: training-aws

training-aws-create-ecr: out/.stamp_training-aws-create-ecr
out/.stamp_training-aws-create-ecr: | $(builder-image)
	$(call make-in-builder,_training-aws-create-ecr)
	$(q)touch $@

ECR_REPO_ARN = arn:aws:ecr:$(AWS_REGION):$(AWS_ACCOUNT_ID):repository/sc-training-aws
_training-aws-create-ecr: $(t)/aws/ECRRepositoryPolicy.json
	@$(quiet) "  CREATE  $(ECR_REPO_ARN)"
	$(q)aws ecr describe-repositories --repository-names sc-training-aws >/dev/null 2>&1 || \
		aws ecr create-repository --repository-name sc-training-aws >/dev/null
	$(q)aws ecr set-repository-policy --repository-name sc-training-aws \
		--policy-text file://$(t)/aws/ECRRepositoryPolicy.json >/dev/null

training-aws-image: out/.stamp_training-aws-image

out/.stamp_training-aws-image: $(t)/Dockerfile.aws $(t)/aws/entrypoint | host-aarch64-registered $(builder-image)
	@$(quiet) "  DOCKBLD sc-training-aws"
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPT) --platform x86_64 -t sc-training-aws -f $(t)/Dockerfile.aws $(t))
	$(q)touch $@

ECR_SERVER = $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com
ECR_IMAGE = $(ECR_SERVER)/sc-training-aws
training-aws-image-ecr-push: out/.stamp_training-aws-image-ecr-push
out/.stamp_training-aws-image-ecr-push: out/.stamp_training-aws-create-ecr out/.stamp_training-aws-image
	@$(quiet) "  DOCKPSH $(ECR_SERVER)/sc-training-aws"
	$(call run-builder,bash -c 'aws ecr get-login-password --region $(AWS_REGION) | \
					docker login --username AWS --password-stdin $(ECR_SERVER) && \
				    docker tag sc-training-aws:latest $(ECR_IMAGE) && \
				    docker push $(ECR_SERVER)/sc-training-aws')
	$(q)touch $@

training-aws: out/.stamp_training-aws

out/.stamp_training-aws: out/.stamp_training-aws-image-ecr-push out/.stamp_aws-s3-create | $(builder-image)
	$(call make-in-builder,_training_aws)
	$(q)touch $@

TRAINING_JOB_NAME := SmartCameraTrainingJob-$(shell date +%s)
_training_aws:
	@$(quiet) "  CREATE  $(TRAINING_JOB_NAME)"
	$(q)aws --region $(AWS_REGION) sagemaker create-training-job \
		--training-job-name $(TRAINING_JOB_NAME) \
		--role-arn arn:aws:iam::$(AWS_ACCOUNT_ID):role/SageMakerRole \
		--algorithm-specification '{ "TrainingInputMode": "File", "TrainingImage": "$(ECR_IMAGE)" }' \
		--output-data-config '{"S3OutputPath": "s3://$(AWS_S3_BUCKET)/SageMaker"}' \
		--resource-config '{"VolumeSizeInGB":60,"InstanceCount":1,"InstanceType":"$(TRAINING_AWS_INSTANCE_TYPE)"}' \
		--stopping-condition '{"MaxRuntimeInSeconds": 5400}'

