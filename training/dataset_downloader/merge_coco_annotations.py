import cv2
import glob
import json
import sys
import threading

def merge_coco_annotations_for_class(class_name, class_id, ann_dict_array):
    ann_dict = {}
    files_done = 0;
    files = glob.glob("out/downloaded_images/"+ class_name + "/*.txt")
    nb_files = len(files)
    print(f'[Thread start] Start processing class {class_name} (id {class_id}) ({nb_files} files)')
    for file in files:
        if (files_done % 200 == 0):
            print(f'[{class_id}:{files_done}/{nb_files}]', end="")
            sys.stdout.flush()
        if (files_done % 1000 == 0):
            print('')
        with open(file, 'r', encoding='utf-8') as read:
            # the key will hold the name the value the content
            ann = read.read()

            file_name = file.split("/")
            file_name = file_name[-1].split(".txt")[0]
            ann = ann.split('\n')
            for an in ann:
                temp = []

                an = an.split()

                xmin = float(an[1])
                ymin = float(an[2])
                xmax = float(an[3])
                ymax = float(an[4])

                temp.append(xmin)
                temp.append(ymin)
                temp.append(xmax)
                temp.append(ymax)
                temp.append(class_id)

            img_file = "out/downloaded_images/"+class_name+"/" +file_name + ".jpg"

            try:
                img = cv2.imread(img_file)
                cv2.imwrite("out/dataset/" + file_name + ".jpg",img)
            except:
                raise

            if file_name not in ann_dict.keys():
                    ann_dict[file_name] = []
                    ann_dict[file_name].append(temp)
            else:
                    ann_dict[file_name].append(temp)
        files_done = files_done + 1
    ann_dict_array[class_id] = ann_dict
    print(f'\n[Thread end] Done processing class {class_name}')

def merge_coco_annotations():
    print('Merging annotations')
    with open('training/dataset_downloader/classes.txt', 'r') as fp:
        lines = fp.readlines()

    class_id = 0
    threads = []
    ann_dict_array = [ {} ] * len(lines)

    for x in lines:
        class_name = x.split('\n')[0].strip()
        t = threading.Thread(target=merge_coco_annotations_for_class, args=(class_name, class_id, ann_dict_array))
        threads.append(t)
        class_id = class_id + 1

    for t in threads:
        t.start()

    ann_dict = {}
    class_id = 0
    for t in threads:
        t.join()
        ann_dict =  {**ann_dict, **ann_dict_array[class_id]}
        class_id = class_id + 1

    with open("out/ann.json", "w") as outfile:
        json.dump(ann_dict, outfile)
