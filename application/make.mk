all: application-image-save

.PHONY: application

application: out/.stamp_application

application-image-save: out/sc-application.tar.bz2

out/.stamp_application: application/config/config.json | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPT) --platform aarch64 -t sc-application application)
	$(q)touch $@

out/sc-application.tar.bz2: out/.stamp_application | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-application | pbzip2 >$@')
