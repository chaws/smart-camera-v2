import cv2
from src.img_helper import preprocess_img

def preprocess_frame(frame, model):
    if model == "tiny_yolov3":
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = preprocess_img(img, (416, 416))
        img = img[0]
    elif model == "yolov3":
        img = cv2.resize(frame, (416, 416))
    else:
        img = cv2.resize(frame, (416, 416))
    return img