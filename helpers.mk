# Makefile helpers

# Definitions to support make / make V=1 / make -s
# - With 'make', full commands may not be displayed, instead they are replaced
#   by a short line
# - With 'make V=1" all commands are displayed
# - With 'make -s' no output is produced except on error
ifneq ($V,1)
q := @
quiet := echo
else
q :=
quiet := true
endif
ifneq ($(filter 4.%,$(MAKE_VERSION)),)  # make-4
ifneq ($(filter %s ,$(firstword x$(MAKEFLAGS))),)
quiet := true
endif
else                                    # make-3.8x
ifneq ($(findstring s, $(MAKEFLAGS)),)
quiet := true
endif
endif

%: %.in conf.mk
	@$(quiet) "  GEN    $@"
	$(q)awk -f subst.awk $< >$@

%.tar.gz: %
	@$(quiet) "  TAR-C  $@"
	$(q)tar czf $@ -C $(dir $<) $(notdir $<)

#
# Allow building aarch64 Docker images on x86_64 host
#

host-arch := $(shell uname -m)

ifneq (,$(filter-out x86_64 aarch64,$(host-arch)))
$(error Unsupported host architecture: $(host-arch))
endif

# would return errors such as: "exec /bin/sh: exec format error".
#
# Note that this step registers binfmt_misc QEMU helpers for architectures
# different from the host, so that foreign arch images can be built and run
# See https://github.com/multiarch/qemu-user-static
ifeq ($(host-arch),x86_64)
host-aarch64-registered: /proc/sys/fs/binfmt_misc/qemu-aarch64

/proc/sys/fs/binfmt_misc/qemu-aarch64:
	@$(quiet) "  DOCKRUN multiarch/qemu-user-static"
	$(q)docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
else
host-aarch64-registered:
endif

# Source (project) directory is mounted under /smart-camera in the container
# It must be possible to override the value to cater for Docker-in-Docker (CI):
# the path has to be the host path (where the Docker daemon runs)
SRCDIR ?= $(CURDIR)
CCACHE_DIR_DOCKER ?= $(shell echo $${CCACHE_DIR-$$HOME/.cache/ccache})
# $(call run-builder,<args>[[,<image name>],<docker args>])
define run-builder
	@$(quiet) "  DOCKRUN $(if $(2),$(2),sc-builder) [$(1)]"
	$(q)docker run --privileged --network host $(3) --rm \
		-v $(SRCDIR):/smart-camera \
		-v $(CCACHE_DIR_DOCKER):/home/sc-user/.cache/ccache \
		-v $(CCACHE_DIR_DOCKER):/home/sc-user/.buildroot-ccache \
		-v /var/run/docker.sock:/var/run/docker.sock \
		$(if $(2),$(2),sc-builder) $(1)
endef

make-in-builder = $(call run-builder,$(MAKE) $(1) $(if $(V),V=$(V),),$(2))

define s3-upload-if-not-exists
	@$(quiet) "  CHECK   s3://$(AWS_S3_BUCKET)/$(notdir $(1))"
	$(q)aws s3 ls s3://$(AWS_S3_BUCKET)/$(notdir $(1)) >/dev/null || { \
		$(quiet) "  S3CP    s3://$(AWS_S3_BUCKET)/$(notdir $(1))"; \
		aws s3 cp $(1) s3://$(AWS_S3_BUCKET); }
endef

define s3-download
	@$(quiet) "  S3CP    $(2)"
	$(q)aws s3 cp s3://$(AWS_S3_BUCKET)/$(1) $(2)
endef

define s3-rm
	@$(quiet) "  S3RM    s3://$(AWS_S3_BUCKET)/$(1)"
	$(q)aws s3 rm s3://$(AWS_S3_BUCKET)/$(1)
endef

define cp
	@$(quiet) "  CP      $2/$(notdir $(1))"
	$(q)cp $1 $2
endef

define cp-r
	@$(quiet) "  CP-R    $2/$(notdir $(1))"
	$(q)cp -R $1 $2
endef

define rm
	@$(quiet) "  RM      $1"
	$(q)rm -f $1
endef

rmsh := $(quiet) "  RM      $1"; rm -f $1

define rmdir
	@$(quiet) "  RMDIR   $1"
	$(q)rmdir $1
endef

define rm-r
	@$(quiet) "  RM-R    $1"
	$(q)rm -rf $1
endef

define mkdir
	@$(quiet) "  MKDIR   $1"
	$(q)mkdir -p $1
endef

define scp
	@$(quiet) "  SCP     root@$2:$(notdir $1)"
	$(q)scp $1 root@$2:
endef

# One file in the model, chosen arbitrarily, to serve in make rules
# The other files are normally generated or downloaded at the same time
compiled-model := out/$(TRAINING_MODEL)/compiled_tvm_model/deploy_graph.json
