# AWS-related target used by several other targets

aws-s3-create: out/.stamp_aws-s3-create

out/.stamp_aws-s3-create: out/.stamp_aws-create-sagemaker-role
	$(call make-in-builder,_aws-s3-create)
	$(q)touch $@

_aws-s3-create: aws/S3BucketPermissions.json
	@$(quiet) "  CREATE  s3://$(AWS_S3_BUCKET)"
	$(q)aws s3 ls s3://$(AWS_S3_BUCKET) >/dev/null 2>&1 || aws s3 mb s3://$(AWS_S3_BUCKET)
	$(q)aws s3api put-bucket-policy --bucket $(AWS_S3_BUCKET) --policy file://aws/S3BucketPermissions.json

aws-create-sagemaker-role: out/.stamp_aws-create-sagemaker-role
out/.stamp_aws-create-sagemaker-role:
	$(call make-in-builder,_aws-create-sagemaker-role)
	$(q)touch $@

_aws-create-sagemaker-role:
	@$(quiet) "  CREATE  SageMakerRole"
	$(q)aws iam get-role --role-name SageMakerRole >/dev/null 2>&1 || \
		aws iam create-role --role-name SageMakerRole \
			--assume-role-policy-document file://$(t)/aws/SageMakerRole.json
	$(q)aws iam attach-role-policy --role-name SageMakerRole \
		--policy-arn arn:aws:iam::aws:policy/AmazonSageMakerFullAccess
