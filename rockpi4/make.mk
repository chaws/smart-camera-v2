#
# Download prebuilt TRS images from:
# https://gitlab.com/Linaro/blueprints/ci#arm-blueprints-ci-images
#

all: rockpi4-trs-images-download

ts-firmware-rockpi4 = out/rockpi4/trs/ts-firmware-rockpi4b.rootfs.wic.gz
#ts-firmware-rockpi4-url = https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b
ts-firmware-rockpi4-url = https://people.linaro.org/~jerome.forissier/trs/20230129/$(notdir $(ts-firmware-rockpi4))

$(ts-firmware-rockpi4): | $(builder-image)
	@$(quiet) "  CURL    $@"
	$(call run-builder,curl --create-dirs -L -o $@ $(ts-firmware-rockpi4-url))

rootfs-rockpi4 = out/rockpi4/trs/trs-image-trs-qemuarm64.rootfs.wic.gz
#rootfs-rockpi4-url = https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm64.rootfs.wic.gz?job=build-meta-trs
rootfs-rockpi4-url = https://people.linaro.org/~jerome.forissier/trs/20230129/$(notdir $(rootfs-rockpi4))

$(rootfs-rockpi4): | $(builder-image)
	@$(quiet) "  CURL    $@"
	$(call run-builder,curl --create-dirs -L -o $@ $(rootfs-rockpi4-url))

rockpi4-trs-images-download: $(ts-firmware-rockpi4) $(rootfs-rockpi4)

rockpi4-trs-firmware-flash:
	@if [ ! "$(DEV)" ]; then \
		echo 'Usage: make $@ DEV=<device path> (such as /dev/mmcblk0)'; \
		false; \
	fi
	sudo sh -c 'zcat $(ts-firmware-rockpi4) >"$(DEV)"'
	sudo sync "$(DEV)"

rockpi4-trs-rootfs-flash:
	@if [ ! "$(DEV)" ]; then \
		echo 'Usage: make $@ DEV=<device path> (such as /dev/sdb)'; \
		false; \
	fi
	sudo sh -c 'zcat $(rootfs-rockpi4) >"$(DEV)"'
	sudo sync "$(DEV)"
	sudo umount "$(DEV)p2" || :
	sudo sgdisk -d 2 "$(DEV)"
	sudo sgdisk -n 2:0:0 "$(DEV)"
	sudo resize2fs "$(DEV)2"
	sudo sync "$(DEV)"
