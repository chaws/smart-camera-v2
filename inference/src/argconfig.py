
def ArgConf(ap):
    ap.add_argument("-p","--port", required=True, help="Port number(not 5000)")
    ap.add_argument("-m","--model", required=True, help="Model to be used tiny_yolov3/yolov3")
    ap.add_argument("-c","--cpu_cores", default="4", required=False, help="CPU cores on the device")
    args = vars(ap.parse_args())
    return args