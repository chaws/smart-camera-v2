import cv2
import numpy as np

def normalize_and_reshape(img):
    img = img.astype(np.float32)                                                        
    img = img / 255.0                                                                   
    img = np.reshape(img, (1,416,416,3))                                                
    return img
    
