all: inference-image-save

.PHONY: inference

i := inference
staging := out/staging/inference

inference: out/.stamp_inference

inference-image-save: out/sc-inference.tar.bz2

out/.stamp_inference-staging: $(compiled-model) \
				out/.stamp_inference-tvm-cross-compile \
				$(i)/Dockerfile $(i)/inference.py \
				$(shell find $(i)/config -type f) \
				$(shell find $(i)/model_data -type f) \
				$(shell find $(i)/src -type f)
	$(call mkdir,$(staging))
	$(call cp,$(i)/Dockerfile,$(staging))
	$(call cp,$(i)/inference.py,$(staging))
	$(call cp-r,$(i)/config,$(staging))
	$(call cp-r,$(i)/model_data,$(staging))
	$(call cp-r,$(i)/src,$(staging))
	$(call cp-r,tvm/python,$(staging)/tvm)
	$(call mkdir,$(staging)/model_data/$(TRAINING_MODEL))
	$(call cp,out/$(TRAINING_MODEL)/compiled_tvm_model/*,$(staging)/model_data/$(TRAINING_MODEL))
	$(q)touch $@

out/.stamp_inference: out/.stamp_inference-staging | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPT) --platform aarch64 -t sc-inference $(staging))
	$(q)touch $@

out/sc-inference.tar.bz2: out/.stamp_inference | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-inference | pbzip2 >$@')

inference-builder-image: out/.stamp_inference-builder-image

out/.stamp_inference-builder-image: $(i)/builder/Dockerfile
	@$(quiet) "  DOCKBLD sc-inference-builder"
	$(q)docker build $(DOCKER_BUILD_OPT) \
		--build-arg HOST_DOCKER_GID=$$(stat --printf=%g /var/run/docker.sock) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		-t sc-inference-builder $(i)/builder
	$(q)touch $@

	$(q)touch $@

inference-tvm-cross-compile: out/.stamp_inference-tvm-cross-compile

out/.stamp_inference-tvm-cross-compile: out/.stamp_inference-builder-image | $(builder-image)
	$(call make-in-builder,_inference-tvm-cross-compile,sc-inference-builder)
	$(q)touch $@

# Note: DESTDIR uses /smart-camera for the top-level directory of the project
# it is how it is mounted in the builder image and the install step wants an
# absolute path
_inference-tvm-cross-compile:
	$(call mkdir,out/tvm_aarch64/build)
	$(call mkdir,out/staging/inference/tvm/destdir)
	$(q)cp tvm/cmake/config.cmake out/tvm_aarch64/build
	$(q)sed -i -e 's/USE_LIBBACKTRACE AUTO/USE_LIBBACKTRACE OFF/' out/tvm_aarch64/build/config.cmake
	$(q)cd out/tvm_aarch64/build && cmake ../../../tvm \
		-DCMAKE_INSTALL_DIR=/ \
		-DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
		-DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc \
		-DCMAKE_CXX_COMPILER=aarch64-linux-gnu-g++ \
		-DCMAKE_FIND_ROOT_PATH=/usr/aarch64-linux-gnu \
		-DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM=NEVER \
		-DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=ONLY \
		-DMACHINE_NAME=aarch64-linux-gnu
	$(q)make -C out/tvm_aarch64/build -j$$(nproc) install DESTDIR=/smart-camera/out/staging/inference/tvm/destdir
