all: capture-image-save

.PHONY: capture

capture: out/.stamp_capture

capture-image-save: out/sc-capture.tar.bz2

out/.stamp_capture: capture/Dockerfile capture/usb_webcam.yml | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPT) --platform aarch64 -t sc-capture capture)
	$(q)touch $@

out/sc-capture.tar.bz2: out/.stamp_capture
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-capture | pbzip2 >$@')
