$(shell mkdir -p out ~/.cache/ccache)

all:

export

.DELETE_ON_ERROR:

include conf.mk

include helpers.mk

clean:
	$(call rm-r,out)
	$(q)find . -name __pycache__ | while read f; do $(quiet) "  RM-R    $$f"; rm -rf $$f; done
	$(call rm,aws/S3BucketPermissions.json)
	$(call rm,training/aws/ECRRepositoryPolicy.json)
	$(call rm,training/aws/entrypoint)
	$(call rm,compilation/aws/sagemaker_config.json)
	$(call rm,capture/usb_webcam.yml)
	$(call rm,sc-demo)

builder-image := out/.stamp_builder-image
builder-image: $(builder-image)

# DOCKER_BUILD_OPT=--no-cache may be useful in case of 404 error
$(builder-image): builder/Dockerfile
	@$(quiet) "  DOCKBLD sc-builder"
	$(q)docker build $(DOCKER_BUILD_OPT) \
		--build-arg HOST_DOCKER_GID=$$(stat --printf=%g /var/run/docker.sock) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		--build-arg HOST_ARCH=$$(uname -m) \
		--build-arg AWS_REGION=$(AWS_REGION) \
		--build-arg AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		--build-arg AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
		-t sc-builder builder
	$(q)touch $@

builder-image-clean:
	$(call rm,$(builder-image))

clean: builder-image-clean

builder-image-run: $(builder-image)
	$(call run-builder,,,-it)

builder-image-run-root: $(builder-image)
	$(call run-builder,,,-u 0:0 -it)

include aws/make.mk
include training/make.mk
include compilation/make.mk
include inference/make.mk
include capture/make.mk
include application/make.mk

include rockpi4/make.mk

all: sc-demo

deploy: out/sc-application.tar.bz2 out/sc-capture.tar.bz2 out/sc-inference.tar.bz2 sc-demo
	@if [ ! "$(TO)" ]; then \
		echo 'Usage: make $@ TO=<board IP or DNS name>'; \
		false; \
	fi
	$(call scp,out/sc-application.tar.bz2,$(TO))
	$(call scp,out/sc-capture.tar.bz2,$(TO))
	$(call scp,out/sc-inference.tar.bz2,$(TO))
	$(call scp,sc-demo,$(TO))
