#!/usr/bin/env python3

'''
Deploying sagmaker neo compiled model on Sagemaker

'''

from sagemaker.tensorflow import TensorFlowModel
import json
import os
import time

cfg_path = os.path.dirname(__file__) + "/sagemaker_config.json"

with open(cfg_path) as json_data_file:
    sagemaker_data = json.load(json_data_file)

input_model_path = sagemaker_data['input_model_path']

role = sagemaker_data['service_role_arn']

compilation_job_name = "sc-{}".format(int(time.time()))

tensorflow_model = TensorFlowModel(model_data=input_model_path, role=role,
                                   framework_version="2.3")

output_path = sagemaker_data['output_model_path']

if input_model_path.endswith(".pb.tar.gz"):
    framework = "tensorflow"
elif input_model_path.endswith(".tflite.tar.gz"):
    framework = "tflite"
elif input_model_path.endswith(".onnx.tar.gz"):
    framework = "onnx"
else:
    raise Exception("Don't know which framework to use for {}"
            .format(input_model_path))

tensorflow_model.compile(
        target_platform_os='LINUX',
        target_platform_arch='ARM64',
        target_platform_accelerator=None,
        #target_platform_accelerator='NVIDIA',
        #compiler_options={'gpu-code': 'sm_62', 'trt-ver': '6.0.1', 'cuda-ver': '10.0'},
        target_instance_family=None,
        input_shape={"input_1": [1, 416, 416, 3]},
        output_path=output_path,
        role=role,
        job_name=compilation_job_name,
        framework=framework)
