c := compilation

.PHONY: compilation
compilation: compilation-aws

compilation-aws: out/$(TRAINING_MODEL)/fused_model.onnx-LINUX_ARM64.tar.gz

compilation-aws-model-upload: out/.stamp_compilation-aws-model-upload

out/.stamp_compilation-aws-model-upload: out/$(TRAINING_MODEL)/fused_model.onnx.tar.gz | $(builder-image)
	$(call make-in-builder,_compilation-aws-model-upload)
	$(q)touch $@

_compilation-aws-model-upload:
	$(call s3-upload-if-not-exists,out/$(TRAINING_MODEL)/fused_model.onnx.tar.gz)

compilation-aws-model-rm: | $(builder-image)
	$(call make-in-builder,_compilation-aws-model-rm)

_compilation-aws-model-rm:
	$(call s3-rm,fused_model.onnx.tar.gz)

compilation-aws-sagemaker: out/.stamp_compilation-aws-sagemaker

out/.stamp_compilation-aws-sagemaker: $(c)/aws/sagemaker_config.json out/.stamp_compilation-aws-model-upload | $(builder-image)
	$(call make-in-builder,_compilation-aws-sagemaker)
	$(q)touch $@

_compilation-aws-sagemaker:
	@$(quiet) "  CHECK   s3://$(AWS_S3_BUCKET)/arm/compiled_models/fused_model.onnx-LINUX_ARM64.tar.gz"
	$(q)aws s3 ls s3://$(AWS_S3_BUCKET)/arm/compiled_models/fused_model.onnx-LINUX_ARM64.tar.gz && \
		echo "Model already compiled! Please run 'make compilation-aws-rm-compiled' if you want to rebuild" || { \
			$(quiet) "  COMPILE s3://$(AWS_S3_BUCKET)/arm/compiled_models/fused_model.onnx-LINUX_ARM64.tar.gz"; \
			python3 $(c)/aws/compile_model.py && \
			echo "Model compiled successfully"; }

compilation-aws-compiled-model-rm: | $(builder-image)
	$(call make-in-builder,_compilation-aws-compiled-model-rm)

_compilation-aws-compiled-model-rm:
	$(call s3-rm,arm/compiled_models/fused_model.onnx-LINUX_ARM64.tar.gz)

out/$(TRAINING_MODEL)/fused_model.onnx-LINUX_ARM64.tar.gz: out/.stamp_compilation-aws-sagemaker | $(builder-image)
	$(call make-in-builder,_compilation-aws-download-compiled-model)
	$(q)touch $@

_compilation-aws-download-compiled-model:
	$(call s3-download,arm/compiled_models/fused_model.onnx-LINUX_ARM64.tar.gz,out/$(TRAINING_MODEL)/fused_model.onnx-LINUX_ARM64.tar.gz)
