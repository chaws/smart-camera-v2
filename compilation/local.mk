c := compilation

.PHONY: compilation
compilation: compilation-local

compilation-local: $(compiled-model)

compilation-local-image: out/.stamp_compilation-local-image

out/.stamp_compilation-local-image: $(c)/local/Dockerfile
	@$(quiet) "  DOCKBLD sc-compilation-local"
	$(q)docker build $(DOCKER_BUILD_OPT) \
		--build-arg HOST_DOCKER_GID=$$(stat --printf=%g /var/run/docker.sock) \
		--build-arg USER_ID=$$(id -u) \
		--build-arg USER_GID=$$(id -g) \
		-t sc-compilation-local -f $(c)/local/Dockerfile $(c)
	$(q)touch $@

$(compiled-model): out/.stamp_compilation-local-image out/$(TRAINING_MODEL)/fused_model.onnx
	$(call run-builder,-p local -if out/$(TRAINING_MODEL)/fused_model.onnx -of $@,sc-compilation-local)
