c := compilation

.PHONY: compilation
compilation: compilation-prebuilt

compilation-prebuilt: $(compiled-model)

out/$(TRAINING_MODEL)/compiled_tvm_model/deploy_graph.json: out/cn-inference-main-model_data.tar.bz2 | $(builder-image)
	$(call mkdir,out/$(TRAINING_MODEL)/compiled_tvm_model)
	@$(quiet) "  TAR-X   out/$(TRAINING_MODEL)/compiled_tvm_model/*"
	$(call run-builder,tar -C out/$(TRAINING_MODEL)/compiled_tvm_model \
		--strip-components=3 -xf $< cn-inference-main-model_data/model_data/$(TRAINING_MODEL))
	$(q)touch $@

out/cn-inference-main-model_data.tar.bz2: | $(builder-image)
	@$(quiet) "  CURL    $@"
	$(call run-builder,curl -o $@ https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/application-microservices/cn-inference/-/archive/main/cn-inference-main.tar.bz2?path=model_data)
